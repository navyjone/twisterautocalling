﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace NVJUtilities
{
    public class Time
    {
        public static string Now()
        {
            return DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        }

        public static bool IsOnTime(string startTime, string endTime)
        {
            var startDt = DateTime.ParseExact(startTime, "HH:mm:ss", CultureInfo.InvariantCulture);
            var endDt = DateTime.ParseExact(endTime, "HH:mm:ss", CultureInfo.InvariantCulture);
            var now = DateTime.Now;
            var result = ((startDt < now) && (now < endDt));
            return result;
        }
    }
}
