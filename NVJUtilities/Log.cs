﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace NVJUtilities
{
    public class Log : EventLog
    {
        private readonly EventLog eventLog;

        public Log(EventLog eventLog)
        {
            this.eventLog = eventLog;
        }

        public void Info(string message)
        {
            this.eventLog.WriteEntry(message, EventLogEntryType.Information);
        }

        public void Warn(string message)
        {
            this.eventLog.WriteEntry(message, EventLogEntryType.Warning);
        }

        public void Error(Exception ex)
        {
            var message = string.Format("Exception: {0} \n\nStack: {1}", ex.Message, ex.StackTrace);
            this.eventLog.WriteEntry(message, EventLogEntryType.Error);
        }

        public void Error (string errorMessage)
        {
            this.eventLog.WriteEntry(errorMessage, EventLogEntryType.Error);
        }
    }
}
