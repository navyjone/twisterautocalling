﻿using System;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using RestSharp;

namespace NVJUtilities
{
    public class API
    {

        public static IRestResponse Send(string actionUrl, string httpMethod, string body)
        {
            try
            {
                return Send(actionUrl, ToMethod(httpMethod), body);
            }
            catch (Exception ex)
            {
                //Log.Error(ex.ToString());
                throw ex;
            }
        }

        public static IRestResponse Send(string actionUrl, Method method, string body)
        {
            try
            {
                var serverUrl = ConfigurationManager.AppSettings["serverUrl"];
                var token = ConfigurationManager.AppSettings["token"];
                var client = new RestClient(serverUrl) { Timeout = int.Parse((ConfigurationManager.AppSettings["callingTimeOut"])) };
                var url = $"{serverUrl}/{actionUrl}";

                var request = new RestRequest(url, method);
                if (!string.IsNullOrEmpty(body))
                {
                    request.AddParameter("application/json; charset=utf-8", body, ParameterType.RequestBody);
                    request.RequestFormat = DataFormat.Json;
                }
                request.AddHeader("Authorization", $"Bearer {token}");
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                var response = client.Execute(request);
                //Log.Info(api + ": " + response.StatusCode);
                //Log.Info(response.Content);
                return response;
            }
            catch (Exception ex)
            {
                //Log.Error(ex.ToString());
                throw ex;
            }
        }

        public static bool IsResponseSuccess(IRestResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK ||
                response.StatusCode == HttpStatusCode.Created ||
                response.StatusCode == HttpStatusCode.NoContent ||
                response.StatusCode == HttpStatusCode.Accepted)
            {
                return true;
            }
            else return false;
        }

        private static Method ToMethod(string httpMethod)
        {
            Method method;
            Enum.TryParse(httpMethod, out method);
            return method;
        }

        //public static async Task<IRestResponse> Get(string api)
        //{
        //    try
        //    {
        //        var serverUrl = ConfigurationManager.AppSettings["serverUrl"];
        //        var token = ConfigurationManager.AppSettings["token"];
        //        var client = new RestClient(serverUrl)
        //        {
        //            Timeout = 5000
        //        };

        //        var request = new RestRequest(api, Method.GET);
        //        request.AddHeader("Authorization", $"Bearer {token}");
        //        ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        //        var response = await client.ExecuteTaskAsync(request);
        //        //Log.Info(api + ": " + response.StatusCode);
        //        //Log.Info(response.Content);
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        //Log.Error(ex.ToString());
        //        throw ex;
        //    }
        //}
    }
}
