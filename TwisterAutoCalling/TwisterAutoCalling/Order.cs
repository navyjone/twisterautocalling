﻿using NVJUtilities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwisterAutoCalling
{
    public class Order
    {
        public Order(string textLine)
        {
            var spliter = new string[] { ConfigurationManager.AppSettings["spliter"] };
            if (textLine.Length == 0 || !textLine.Contains(spliter[0])) throw new Exception("file misformat");
            var columns = textLine.Split(spliter, StringSplitOptions.RemoveEmptyEntries);

            Status = columns[(int)Column.status];
            ExecuteDate = columns[(int)Column.dateTime];
            ActionUrl = columns[(int)Column.actionUrl];
            HttpMethod = columns[(int)Column.httpMethod];
            Body = columns.Length == 5 ? columns[(int)Column.body] : null;
        }

        public class Statuses
        {
            public const string Executed = "EXECUTED";
            public const string Pending = "PENDING";
            public const string Error = "ERROR";
        }
        public enum Column { status, dateTime, actionUrl, httpMethod, body}

        public string Status { get; set; }
        public string ExecuteDate { get; set; }
        public string ActionUrl { get; set; }
        public string HttpMethod { get; set; }
        public string Body { get; set; }

        public bool IsExecuted ()
        {
            return this.Status == Statuses.Executed;
        }

        public bool IsError ()
        {
            return this.Status == Statuses.Error;
        }

        public bool IsOnExecuteTime()
        {
            return DateTime.Now >= DateTime.Parse(ExecuteDate);
        }

        public string ToLine()
        {
            var props = new List<string>
            {
                this.Status,
                this.ExecuteDate,
                this.ActionUrl,
                this.HttpMethod,
                this.Body
            };
            return string.Join(ConfigurationManager.AppSettings["spliter"], props);
        }
    }
}
