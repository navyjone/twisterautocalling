﻿using Newtonsoft.Json;
using NVJUtilities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TwisterAutoCalling
{
    public partial class TwisterAutoCalling : ServiceBase
    {
        private readonly string eventName = ConfigurationManager.AppSettings["eventName"];
        private readonly string sourceFolder = ConfigurationManager.AppSettings["sourceFolder"];
        private readonly string executedFolder = ConfigurationManager.AppSettings["executedFolder"];
        private readonly string errorFolder = ConfigurationManager.AppSettings["errorFolder"];
        private readonly string errorNotiActionUrl = ConfigurationManager.AppSettings["errorNotiActionUrl"];
        private Log log;

        public TwisterAutoCalling()
        {
            try
            {
                InitializeComponent();
                Initial();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", ex.ToString(), EventLogEntryType.Error);
                StopService();
            }
        }

        public void Debug()
        {
            OnStart(new string[0]);
            while (true) ;
        }

        protected override void OnStart(string[] args)
        {
            this.log.Info("Service started");
            ScanFile();
            Timer timer = new Timer { Interval = int.Parse(ConfigurationManager.AppSettings["interval"]) };
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        protected override void OnStop()
        {
            log.Info("Service is stopped");
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {
             ScanFile();
        }

        private void Initial()
        {
            eventLog = new EventLog();
            if (!EventLog.SourceExists(eventName)) EventLog.CreateEventSource(eventName, eventName);
            eventLog.Source = eventName;
            eventLog.Log = eventName;
            this.log = new Log(eventLog);
            DirectoryCheck();
        }

        private void DirectoryCheck()
        {
            if (!Directory.Exists(sourceFolder))
            {
                log.Info("Create new empty source folder from config.");
                Directory.CreateDirectory(sourceFolder);
            }

            if (!Directory.Exists(executedFolder))
            {
                log.Info("Create new empty executed folder from config.");
                Directory.CreateDirectory(executedFolder);
            }

            if (!Directory.Exists(errorFolder))
            {
                log.Info("Create new empty error folder from config.");
                Directory.CreateDirectory(errorFolder);
            }
        }

        private void ScanFile()
        {
            var filePaths = Directory.GetFiles(sourceFolder);
            if (filePaths.Length == 0) return;
            foreach (var filePath in filePaths)
            {
                FileStream file = null;
                var lines = new string[0];
                try
                {
                    // lock file
                    lines = File.ReadAllLines(filePath);
                    file = File.Open(filePath, FileMode.Open);                  
                }
                catch
                {
                    // some thread opening file then next file
                    continue;
                }

                try
                {
                    var orders = new List<Order>();
                    var isErrorOccured = false;
                    foreach (var line in lines)
                    {
                        var order = new Order(line);
                        if (isErrorOccured || order.IsExecuted() || !order.IsOnExecuteTime())
                        {
                            orders.Add(order);
                            continue;
                        }

                        var response = API.Send(order.ActionUrl, order.HttpMethod, order.Body);
                        if (API.IsResponseSuccess(response))
                        {
                            order.Status = Order.Statuses.Executed;
                        }
                        else
                        {
                            order.Status = Order.Statuses.Error;
                            SendErrorNoti(filePath);
                            isErrorOccured = true;
                            log.Error($"{order.ActionUrl} : Response {response.StatusCode}");
                        }

                        orders.Add(order);
                    }

                    //unlock file
                    file.Close();

                    UpdateFile(filePath, orders);
                    if (isErrorOccured) Move(filePath, errorFolder);
                    if (IsAllExecuted(orders)) Move(filePath, executedFolder);
                }
                catch (Exception ex)
                {
                    file.Close();
                    Move(filePath, errorFolder);
                    log.Error(ex);
                    continue;
                }
            }
        }

        private void UpdateFile(string filePath, List<Order> orders)
        {
            var lines = new List<string>();
            foreach (var order in orders) lines.Add(order.ToLine());
            File.WriteAllLines(filePath, lines.ToArray());
        }

        private void Move(string filePath, string destinationFolder)
        {
            var fileName = Path.GetFileName(filePath);
            var destinationFile = Path.Combine(destinationFolder, fileName);
            File.Move(filePath, destinationFile);
        }

        private bool IsAllExecuted(List<Order> orders)
        {
            var isAllExecuted = true;
            foreach (var order in orders) isAllExecuted &= order.IsExecuted();
            return isAllExecuted;
        }

        private void SendErrorNoti(string filePath)
        {
            var body = new Dictionary<string, object>
            {
                {"fileName",  Path.GetFileName(filePath) }
            };
            API.Send(errorNotiActionUrl, Method.POST, JsonConvert.SerializeObject(body));
        }

        public void StopService()
        {
            var service = new ServiceController(this.ServiceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(int.Parse(ConfigurationManager.AppSettings["stopServiceTimeOut"]));

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}
