﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TwisterAutoCalling
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            // Run when click start button
            if (Environment.UserInteractive)
            {
                var twisterAutoCalling = new TwisterAutoCalling();
                twisterAutoCalling.Debug();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new TwisterAutoCalling()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
